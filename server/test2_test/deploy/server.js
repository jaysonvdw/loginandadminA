
"use strict";
  //# ag3 holds the 'global' variables; they are mostly for debugging purposes.
  var ag3 = []; ag3["appName"] = "test2";

    
  //# shortcut for console.log
  var slog = function(t) {
    var txt;
    if (typeof t == 'object') {
         txt = (JSON && JSON.stringify ? JSON.stringify(t) : t);
     } else {
         txt = t;
     }
  }


  //# create express server for 9801
  var fs = require('fs');
  var https = require('https');
  var app = require('express')();

  
    var options = {
        key  : fs.readFileSync('../server.key'),
        cert : fs.readFileSync('../server.crt')
    };
        

  var WebSocketServer = require('ws').Server
    , https = require('https')
    , express = require('express')
    , app = express();


  var bodyParser = require('body-parser')
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: true
  }));

  //# for testing only: ignore unauthorized certificate. REMOVE FOR PRODUCTION!
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

  var port = 9801;
  var server = https.createServer(options, app);
  server.listen(port);
  slog("This is simple_server_8000 (running on port " + port + ").")



  //# can be safely deleted:
  slog("Hello, Server! from: " + ag3["appName"]);

  //# define schema
  var schema = ["_id","txt_name", "txt_q1", "txt_q2", "txt_q3"];

  //# initialize diskdb
  var db = require('diskdb');
  db = db.connect('.', ['test2_test']);
  var thisDb = db.test2_test;
  //
  //




  //# START of CSV and user specific code
  var records;
  //# ref: http://stackoverflow.com/a/1293163/2343
  //# parse csv
  var CSVToArray = function ( strData, strDelimiter ){
      strDelimiter = (strDelimiter || ",");

      var objPattern = new RegExp(
          (
              "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
              "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
              "([^\"\\" + strDelimiter + "\\r\\n]*))"
          ),
          "gi"
          );

      var arrData = [[]];
      var arrMatches = null;

      while (arrMatches = objPattern.exec( strData )){

          var strMatchedDelimiter = arrMatches[ 1 ];
          if (
              strMatchedDelimiter.length &&
              strMatchedDelimiter !== strDelimiter
              ){
              arrData.push( [] );
          }

          var strMatchedValue;
          if (arrMatches[ 2 ]){
              strMatchedValue = arrMatches[ 2 ].replace(
                  new RegExp( "\"\"", "g" ),
                  "\""
                  );
          } else {
              strMatchedValue = arrMatches[ 3 ];
          }
          arrData[ arrData.length - 1 ].push( strMatchedValue );
      }
      return( arrData );
  }

  var isNumeric = function (n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  var loadUsers = function () {
    var fs = require('fs');
    var text = fs.readFileSync('users.csv', 'utf8');
    var arr = CSVToArray(text);

    //# turn CSV to JSON
    var json = [];
    for (var j = 0; j < arr.length; j++){
        var t = {};
        if ( isNumeric(arr[j][0] ) ){
          t['id'] = arr[j][0];
          t['username'] = arr[j][1];
          t['password'] = arr[j][2];
          t['displayname'] = arr[j][3];
          t['email'] = arr[j][4];
          json.push(t)
        }
    }
    return json;
  }

  records = loadUsers();

  //# create REST function
  app.get('/loadusers', function(req, res) {
    slog('loadusers csv.');
    records = loadUsers();
    res.send("csv loaded.");
  });

  //# END CSV and user specific code



  //# START of passport specific code
  var passport = require('passport');
  var Strategy = require('passport-local').Strategy;

  loadUsers();

  var findById = function(id, cb) {
    process.nextTick(function() {
      var idx = id - 1;
      if (records[idx]) {
        cb(null, records[idx]);
      } else {
        cb(new Error('User ' + id + ' does not exist'));
      }
    });
  }

  var findByUsername = function(username, cb) {
    process.nextTick(function() {
      for (var i = 0, len = records.length; i < len; i++) {
        var record = records[i];
        if (record.username === username) {
          return cb(null, record);
        }
      }
      return cb(null, null);
    });
  }

  //
  //
  //

  //# hardcoded login html code - this should be changed
  var login = function(query) {
    return `
  <form action="/login?name=` + query +`" method="post">
  	<div>
      <label>Username:</label><input type="text" name="username" value="` + query +`"/><br/>
  	</div>
  	<div>
      <label>Password:</label><input type="password" name="password" value="d1"/>
  	</div>
  	<div><input type="submit" value="Submit"/></div>
  </form>
  `}

  //
  //
  //
  //

  //
  passport.use(new Strategy(
    function(username, password, cb) {
      findByUsername(username, function(err, user) {
        if (err) { return cb(err); }
        if (!user) { return cb(null, false); }
        if (user.password != password) { return cb(null, false); }
        return cb(null, user);
      });
    }));

  //
  passport.serializeUser(function(user, cb) {
    cb(null, user.id);
  });

  passport.deserializeUser(function(id, cb) {
    findById(id, function (err, user) {
      if (err) { return cb(err); }
      cb(null, user);
    });
  });

  app.use(require('cookie-parser')());
  app.use(require('body-parser').urlencoded({ extended: true }));
  app.use(require('express-session')({
    secret: 'keyboard cat', resave: false, saveUninitialized: false
  }));

  app.use(passport.initialize());
  app.use(passport.session());

  app.get('/',
    function(req, res) {
      var querystring = req.url.substr(req.url.indexOf('?') + 1);
      if (req.user == null) {
        //* user is not logged in, send index, which will display only the login button
        res.sendFile(__dirname + '/index.html');

      }
      else {
        //* user is logged in, redirect to content

        res.redirect(301, "content?" + querystring);
      }
    });
    //

      //

  app.get('/login',
    function(req, res){
      var querystring = req.url.substr(req.url.indexOf('?') + 1);

      var thisName = "";
      //# do we have a name?
      if (req.query.name) thisName = req.query.name.split("=")[0];
      //# send login form with username if provided
      res.send(login(thisName));
    });

  app.post('/login',
    passport.authenticate('local', { failureRedirect: '/login' }),
      function(req, res) {
        res.redirect('/?name='+req.body.username);
      });

  app.get('/logout',
    function(req, res){
      req.logout();
      res.redirect('/');
    });
  //

  app.get('/content',
    require('connect-ensure-login').ensureLoggedIn(),
    function(req, res) {
      var querystring = req.url.substr(req.url.indexOf('?') + 1);
      slog("route content: " + querystring);
      res.sendFile(__dirname + '/index.html');
    });
    //# END passport specific code



  //# START of the server side of the REST call code

  //# The nice thing about REST is: it can be cURLed.
  //# get all data as JSON with
  //# https://localhost:8000/data gives the JSON of all data
  //# post data with
  //# curl -X POST -H "Content-Type: application/json" -d '{"data1":"val"}' http://localhost:8000/data

  app.get('/data', function(req, res) {
    ag3["datas"] = thisDb.find().reverse();
    res.json(ag3["datas"]);
  });

  app.get('/add', function(req, res) {
    slog('received get/add.');
    slog(req.query);
    thisDb.save(req.query);
    res.sendfile("index.html");
  });

  app.post('/data', function(req, res) {
    slog('received post/data.');
    slog(req.body);
    res.json(thisDb.save(req.body));
  });

  app.put('/data', function(req, res) {
    slog('received put/data.');
    slog(req.body);
    res.json(thisDb.update(req.body.old, req.body.new));
  });

  app.delete('/data', function(req, res) {
    res.json(thisDb.remove(req.body));
  });
  //# END REST call code



  //# START non generic code
  var reset = function() {
    thisDb.remove();
    db = db.connect('.', ['test2_test']);
    //
    //
  }

  //
  //
  //
  //
  //
  //

  app.get('/reset', function(req, res) {
    slog('received get/reset.');
    reset();

    wss.clients.forEach(function each(client) {
      client.send(JSON.stringify({ channel:ag3["appName"], data: "update" }));
    });
  });
  //# END non generic code




  //# START show csv code
  app.get('/csv', function(req, res) {
    slog('received get/csv.');

    var fs = require('fs');
    var json = JSON.parse(fs.readFileSync('test2_test.json', 'utf8'));
    var arr = json;


    var temp = "";
    schema.forEach(function(i) {
      temp += "" + i + ",";
    });
    temp += "\n";

    for (var j = 0; j < arr.length; j++){
      schema.forEach(function(i) {
        temp += "" + arr[j][i] + ",";
      });
      temp += "\n";
    }

    var result = temp;
    temp = "";
    temp += "<textarea rows=4 cols=50>";
    temp += result;
    temp += "</textarea>";
    temp += "<br />";
    temp += result;


    res.send(temp);

  });
  //# END show csv code



  //# START aframe specific code
  app.post('/answer', function(req, res) {
    slog('received post/answer.');


    var a = thisDb.findOne({"txt_name":req.body.txt_name});
    if (a) {
      var b = thisDb.update({"_id": a._id}, req.body, {multi: false, upsert: true})
    } else {
      var b = thisDb.save(req.body)
    }
    slog(b);
    res.json( a );
  });
  //# END aframe specific code




  //# START hot_update code
  var WebSocketServer = require('ws').Server
    , wss = new WebSocketServer({ server: server});

  wss.on('connection', function connection(ws) {
      ws.on('message', function incoming(messageTxt) {
        var messageObj = JSON.parse(messageTxt);
        if (messageObj.channel == ag3["appName"]) {
          var data= messageObj.data;
          wss.clients.forEach(function each(client) {
            client.send(messageTxt);
          });
        }
      });
  });
  //# END hot_update code





